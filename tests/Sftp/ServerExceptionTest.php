<?php
namespace CodeClouds\FtpClient\Tests;

/**
 * Test of Server Exceptions
 *
 * @package FtpClient
 * @subpackage Tests
 * @author altayalp <altayalp@gmail.com>
 */
class ServerExceptionTest extends \PHPUnit_Framework_TestCase {
    
    /**
     * @expectedException \altayalp\FtpClient\Exceptions\ConnectionFailedException
     */
    public function testConnectException() {
        @$server = new \CodeClouds\FtpClient\Servers\SftpServer('xx.xx.com');
    }
    
}
