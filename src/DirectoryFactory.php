<?php

/*
 * The sofware is provided under Mit License.
 * For the full copyright and license information, please view the LICENSE file
 */

namespace CodeClouds\FtpClient;

use CodeClouds\FtpClient\Interfaces\ServerInterface;
use CodeClouds\FtpClient\Interfaces\FactoryInterface;
use CodeClouds\FtpClient\Servers\FtpServer;
use CodeClouds\FtpClient\Servers\SftpServer;
use CodeClouds\FtpClient\Directories\FtpDirectory;
use CodeClouds\FtpClient\Directories\SftpDirectory;

/**
 * Factory for Directory classes
 *
 * @author CodeClouds
 */
class DirectoryFactory implements FactoryInterface
{
    
    /**
     * Build method for Directory classes
     */
    public static function build(ServerInterface $server)
    {
        if ($server instanceof SftpServer) {
            return new SftpDirectory($server);
        } elseif ($server instanceof FtpServer || $server instanceof SslServer) {
            return new FtpDirectory($server);
        } else {
            throw new \InvalidArgumentException('The argument is must instance of server class');
        }
    }
    
}
