<?php

/*
 * The sofware is provided under Mit License.
 * For the full copyright and license information, please view the LICENSE file
 */

namespace CodeClouds\FtpClient\Exceptions;

use CodeClouds\FtpClient\Abstracts\AbstractFtpException;

/**
 * Throw when ftp extension missing
 *
 * @author altayalp <altayalp@gmail.com>
 * @package FtpClient
 * @subpackage Exceptions
 */
class ExtensionMissingException extends AbstractFtpException
{
    
}
